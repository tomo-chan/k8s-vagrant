# Kubernetes on Vagrant

## Preinstall

1. Install Virtualbox

1. Install Vagrant

## Specification

* VM OS : ubuntu 16.04
* Network
    * VM network : 172.16.0.0/24
    * Pod network : 192.168.0.0/16

## How to setup

1. Install k8s envirnnment

    ```shell
    git clone git@gitlab.com:tomo-chan/k8s-vagrant.git
    cd k8s-vagrant
    vagrant up
    ```

1. log into master server

    ```shell
    vagrant ssh k8s-master
    ```

## Customize environment

1. VM Spec

    Edit Vagrantfile

    * N : Number of worker nodes
    * v.memory : Memory size of VMs
    * v.cpus : Number of CPUs of VMs
    * node_ip : IP address of VMs

1. Ansible variables

    Edit resources in roles