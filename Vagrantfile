IMAGE_NAME = "bento/ubuntu-18.04"
N = 3

def add_disk(define, vm_name, disk_name)
    line = `VBoxManage list systemproperties`.split(/\n/).grep(/Default machine folder/).first
    vb_machine_folder = line.split(':', 2)[1].strip()

    disk_path = File.join(vb_machine_folder, vm_name, disk_name)
    unless File.exist?(disk_path)
        define.vm.provider :virtualbox do |v|
            v.name = vm_name
            v.customize [
                "createmedium", "disk",
                "--filename", disk_path,
                "--format", "VDI",
                "--size", 10 * 1024]
            
            v.customize [
                "storageattach", :id,
                "--storagectl", "SATA Controller",
                "--port", 1,
                "--device", 0,
                "--type", "hdd",
                "--medium", disk_path]
        end
    end
end

Vagrant.configure("2") do |config|
    config.ssh.insert_key = false

    config.vm.provider "virtualbox" do |v|
        v.memory = 4192
        v.cpus = 2
    end
      
    config.vm.define "k8s-master" do |master|
        vm_name = "k8s-master"
        add_disk(master, vm_name, "sdb.vdi")

        master.vm.box = IMAGE_NAME
        master.vm.network "private_network", ip: "172.16.0.10"
        master.vm.hostname = vm_name
        master.vm.provision "ansible" do |ansible|
            ansible.playbook = "kubernetes-setup/master-playbook.yml"
            ansible.extra_vars = {
                node_ip: "172.16.0.10",
            }
        end
    end

    (1..N).each do |i|
        config.vm.define "node-#{i}" do |node|
            vm_name = "node-#{i}"
            add_disk(node, vm_name, "sdb.vdi")

            node.vm.box = IMAGE_NAME
            node.vm.network "private_network", ip: "172.16.0.#{i + 10}"
            node.vm.hostname = vm_name
            node.vm.provision "ansible" do |ansible|
                ansible.playbook = "kubernetes-setup/node-playbook.yml"
                ansible.extra_vars = {
                    node_ip: "172.16.0.#{i + 10}",
                }
            end
        end
    end
end
